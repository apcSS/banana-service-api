package com.banana.bananaserviceapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BananaServiceApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BananaServiceApiApplication.class, args);
	}

}
